import { Controller, Delete, Get } from '@nestjs/common';

@Controller('usuario') //path principal: http://localhost:3000/usuario
export class UsuarioController {

    @Get('obtener-usuarios') //path: http://localhost:3000/usuario/obtener-usuarios
    obtenerUsuarios(){
        return{
            mensaje: 'TODOS los usuarios'
        }
    }

    @Delete('eliminar-usuario') //path : http://localhost:3000/usuario/eliminar-usuario
    eliminarUsuarios(){
        return{
            mensaje: 'usuario eliminado'
        }
    }

}
